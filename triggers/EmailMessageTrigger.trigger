trigger EmailMessageTrigger on EmailMessage (before insert, after insert, after update) {
    if (Trigger.isAfter && Trigger.isInsert) {
        EmailMessageTriggerHandler.afterInsert(Trigger.new);
    }

    if (Trigger.isAfter && Trigger.isUpdate) {
        EmailMessageTriggerHandler.afterUpdate(Trigger.new);
    }

    if (Trigger.isBefore && Trigger.isInsert) {
        EmailMessageTriggerHandler.beforeInsert(Trigger.new);
    }
}