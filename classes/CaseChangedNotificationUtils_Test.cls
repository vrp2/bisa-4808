@IsTest
private with sharing class CaseChangedNotificationUtils_Test {
    @TestSetup
    static void setupTestData() {
        CaseCustomSettingUtil.customSettingMethod();

        CaseChangedNotification_Excluded_Profile__c cs = new CaseChangedNotification_Excluded_Profile__c(Name = 'Integration API Profile - Basic');
        insert cs;

        Id recordTypeId = [
                SELECT Name, SobjectType,IsPersonType
                FROM RecordType
                WHERE SobjectType='Account'
                AND IsPersonType = true
                ORDER BY CreatedDate
                LIMIT 1
        ].Id;

        Account newAccount = new Account (
                FirstName = 'Test Account',
                LastName = 'Account',
                RecordTypeId = recordTypeId,
                PersonEmail = 'test7@test.com',
                RG_Level__c = 'Level 1',
                AML_Rating__c = 'Rating',
                KYC_Status__c = 'Status',
                Segmentation__c = 'VIP',
                Language__c = 'EN'
        );
        insert newAccount;

        Contact newContact = new Contact (
                FirstName = 'Test Test',
                LastName = 'Contact',
                Email = 'test7@test.com'
        );
        insert newContact;

        Case newCase = new Case(
                Subject = 'TestSubject',
                Brand__c = 'Tipico',
                Language__c = 'English',
                Status = 'Open',
                SuppliedEmail = 'testEmail@test.com',
                AccountId = newAccount.Id,
                ContactId = newContact.Id,
                RecordTypeId = [
                        SELECT Id
                        FROM RecordType
                        WHERE DeveloperName = 'Malta_Customer_Support'
                        AND SobjectType = 'Case'
                ].Id,
                OwnerId = [
                        SELECT Id
                        FROM User
                        WHERE UserRole.Name = 'CO Agent'
                        AND IsActive = TRUE
                        LIMIT 1
                ].Id
        );

        insert newCase;
    }

    @IsTest
    private static void emailSent_Test() {
        Case testCase = [
                SELECT Id, CaseNumber, Username__c, CreatedById
                FROM Case
                LIMIT 1
        ];

        Test.startTest();

        EmailMessage newMessage = new EmailMessage();
        newMessage.FromAddress = 'test@test.com';
        newMessage.CcAddress = 'test2@test.com';
        newMessage.FromName = 'Mr. Test';
        newMessage.Headers = 'test';
        newMessage.HtmlBody = 'test';
        newMessage.TextBody = 'test';
        newMessage.ParentId = testCase.Id;
        newMessage.Status = '3';
        newMessage.Subject = 'test';
        newMessage.ToAddress = 'test3@test.com';
        newMessage.Incoming = false;
        insert newMessage;

        Test.stopTest();

        Test.getEventBus().deliver();
    }

    @IsTest
    private static void emailSentByExcludedProfile_Test() {
        List<Profile> listProfile = [SELECT Id FROM Profile WHERE Name = 'Integration API Profile - Basic'];
        Profile testProfile = new Profile();

        if (listProfile.isEmpty() == false) {
            testProfile = listProfile[0];
        }

        User testUser = [SELECT Name FROM User WHERE ProfileId = :testProfile.Id AND Name = 'Tipico Customer Care'];

        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        System.runAs(testUser) {

            Case testCase = new Case(
                    Subject = 'TestSubject',
                    Brand__c = 'Tipico',
                    Language__c = 'English',
                    Status = 'Open',
                    SuppliedEmail = 'testEmail@test.com',
                    AccountId = acc.Id,
                    ContactId = contact.Id,
                    RecordTypeId = [
                            SELECT Id
                            FROM RecordType
                            WHERE DeveloperName = 'Malta_Customer_Support'
                            AND SobjectType = 'Case'
                    ].Id
            );

            insert testCase;

            Test.startTest();

            EmailMessage newMessage = new EmailMessage();
            newMessage.FromAddress = 'test@test.com';
            newMessage.CcAddress = 'test2@test.com';
            newMessage.FromName = 'Mr. Test';
            newMessage.Headers = 'test';
            newMessage.HtmlBody = 'test';
            newMessage.TextBody = 'test';
            newMessage.ParentId = testCase.Id;
            newMessage.Status = '3';
            newMessage.Subject = 'test';
            newMessage.ToAddress = 'test3@test.com';
            newMessage.Incoming = false;
            insert newMessage;

            Test.stopTest();

            Test.getEventBus().deliver();
        }
    }

    @IsTest
    private static void setPushTopic_Test() {
        String pushTopicName = String.valueOf(Datetime.now().getTime());
        CaseChangedNotificationUtils.EVENT_NAMES_AND_TYPES = new Map<String, String>{pushTopicName => CaseChangedNotificationUtils.CASE_CHANGED_EVENT_TYPE};

        CaseChangedNotificationUtils.setPushTopics();
        List<PushTopic> pushTopics = [
                SELECT Id, Name
                FROM PushTopic
        ];

        System.assert(pushTopics.size() == 1);
    }

    @IsTest
    private static void updatePushTopic_Test() {
        String pushTopicName = String.valueOf(Datetime.now().getTime());
        CaseChangedNotificationUtils.EVENT_NAMES_AND_TYPES = new Map<String, String>{pushTopicName => CaseChangedNotificationUtils.CASE_CHANGED_EVENT_TYPE};
        String query = 'SELECT Id, CaseNumber From Case';

        PushTopic pt = new PushTopic (
                ApiVersion = 50.0,
                IsActive = true,
                Name = pushTopicName,
                NotifyForOperationUpdate = true,
                Query = query
        );

        insert pt;

        CaseChangedNotification_Excluded_Profile__c excludedProfile = new CaseChangedNotification_Excluded_Profile__c(
                Name = 'CHC_Tipico Profile'
        );

        insert excludedProfile;

        CaseChangedNotificationUtils.updatePushTopics();
        List<PushTopic> pushTopicsUpd = [
                SELECT Id, Name, Query
                FROM PushTopic
                WHERE Name = :pushTopicName
        ];

        System.assert(pushTopicsUpd.size() == 1);
        System.assert(pushTopicsUpd[0].Query.contains(' WHERE '));
    }

    @isTest
    static void createPushTopicWithCustomFieldsTest() {
        List<CCNotification_CaseFields__c> customFields = new List<CCNotification_CaseFields__c>();
        CCNotification_CaseFields__c  customField1 =  new CCNotification_CaseFields__c(Name = 'OwnerId', Apply_For_Case_Changed_Event__c = true, Apply_For_Email_Received_Event__c = true);
        CCNotification_CaseFields__c  customField2 =  new CCNotification_CaseFields__c(Name = 'Status', Apply_For_Case_Changed_Event__c = false, Apply_For_Email_Received_Event__c = false);
        customFields.add(customField1);
        customFields.add(customField2);
        insert customFields;

        String pushTopicName = String.valueOf(Datetime.now().getTime());
        String pushTopicName2 = String.valueOf(Datetime.now().getTime() + 5);
        CaseChangedNotificationUtils.EVENT_NAMES_AND_TYPES = new Map<String, String>{pushTopicName => CaseChangedNotificationUtils.CASE_CHANGED_EVENT_TYPE, pushTopicName2 => CaseChangedNotificationUtils.EMAIL_RECEIVED_EVENT_TYPE};

        CaseChangedNotificationUtils.setPushTopics();
        List<PushTopic> pushTopics = [
                SELECT Id, Name, Query
                FROM PushTopic
        ];

        System.assert(pushTopics.size() == 2);
        for (PushTopic pt : pushTopics) {
            System.assert(pushTopics[0].Query.contains('OwnerId'));
            System.assert(pushTopics[0].Query.contains('Status') == false);
        }
    }

    @isTest
    static void createPushTopicsWithCustomFiltersTest() {
        List<CCNotification_Filters__c> customFilters = new List<CCNotification_Filters__c>();
        CCNotification_Filters__c  customFilter1 =  new CCNotification_Filters__c(Name = 'statement1', Apply_For_Case_Changed_Event__c = true, Apply_For_Email_Received_Event__c = true, Filter_Condition__c = 'Status = \'Closed\'');
        CCNotification_Filters__c  customFilter2 =  new CCNotification_Filters__c(Name = 'statement2', Apply_For_Email_Received_Event__c = false, Filter_Condition__c = 'Status != \'Open\'');
        customFilters.add(customFilter1);
        customFilters.add(customFilter2);
        insert customFilters;

        String pushTopicName = String.valueOf(Datetime.now().getTime());
        String pushTopicName2 = String.valueOf(Datetime.now().getTime() + 5);
        CaseChangedNotificationUtils.EVENT_NAMES_AND_TYPES = new Map<String, String>{pushTopicName => CaseChangedNotificationUtils.CASE_CHANGED_EVENT_TYPE, pushTopicName2 => CaseChangedNotificationUtils.EMAIL_RECEIVED_EVENT_TYPE};

        CaseChangedNotificationUtils.setPushTopics();
        List<PushTopic> pushTopics = [
                SELECT Id, Name, Query
                FROM PushTopic
        ];

        System.assert(pushTopics.size() == 2);

        for (PushTopic pt : pushTopics) {
                System.assert(pushTopics[0].Query.contains('AND'));
                System.assert(pushTopics[0].Query.contains('OR') == false);
                System.assert(pushTopics[0].Query.contains('Closed'));
                System.assert(pushTopics[0].Query.contains('Open') == false);
        }
    }

    @isTest
    static void logExceptionOnSetRecordsTest() {
        Integer logsAmount = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmount == 0);

        setPushTopic_Test();
        CaseChangedNotificationUtils.setPushTopics();

        Integer logsAmountToCheck = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmountToCheck == 1);
    }

    @isTest
    static void logExceptionOnUpdatePushTopicWithoutSetTest() {
        Integer logsAmount = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmount == 0);

        CaseChangedNotificationUtils.updatePushTopics();
        List<PushTopic> pushTopics = [
                SELECT Id, Name, Query
                FROM PushTopic
        ];

        System.assert(pushTopics.size() == 0);
        Integer logsAmountToCheck = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmountToCheck == 1);
    }

    @isTest
    static void logExceptionOnSetPushTopicWithWrongQueryTest() {


        CCNotification_Filters__c  customFilter1 =  new CCNotification_Filters__c(Name = 'statement1', Filter_Condition__c = 'Status = \'Closed\'');
        insert customFilter1;

        String pushTopicName = String.valueOf(Datetime.now().getTime());
        CaseChangedNotificationUtils.EVENT_NAMES_AND_TYPES = new Map<String, String>{pushTopicName => CaseChangedNotificationUtils.CASE_CHANGED_EVENT_TYPE};

        CaseChangedNotificationUtils.setPushTopics();
        List<PushTopic> pushTopics = [
                SELECT Id, Name, Query
                FROM PushTopic
        ];

        System.assert(pushTopics.size() == 1);

        CCNotification_Filters__c  customFilter2 =  new CCNotification_Filters__c(Name = 'statement1', Filter_Condition__c = 'Status != \'Closed\'');
        insert customFilter2;

        Integer logsAmount = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmount == 0);

        CaseChangedNotificationUtils.updatePushTopics();

        Integer logsAmountToCheck = [SELECT COUNT() FROM ExceptionLogger__c];
        System.assert(logsAmountToCheck == 1);
    }

}