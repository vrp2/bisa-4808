/***********
Author:     Olga Chaplinskaya - VRP
Purpose:    Utility methods for the Case Changed Notification Aura Component. Created and maintained by VRP.
JIRA:       CW-290 and CW-307
***********/

public with sharing class CaseChangedNotificationCtrl {
    @AuraEnabled(Cacheable=true)
    public static String getLastModifiedUserName(String userId){
        String userName = '';
        if (userId != null) {
            List<User> users = [
                    SELECT Name
                    FROM User
                    WHERE Id = :userId
            ];

            if (users.isEmpty() == false) {
                return users[0].Name;
            }
        }

        return userName;
    }
}