/***********
Author:     Olga Chaplinskaya - VRP
Purpose:    Utility methods for the Case Changed Notification Aura Component. Created and maintained by VRP.
JIRA:       CW-290 and CW-307
***********/

// the functionality was implemented for CW-290 and CW-307 tickets
public without sharing class CaseChangedNotificationUtils {
    private static final Set<String> EXCLUDED_PROFILES = CaseChangedNotification_Excluded_Profile__c.getAll().keySet();
    private static final String PROFILE_NAME = [SELECT Name FROM Profile WHERE Id = : UserInfo.getProfileId()].Name;
    private static final String CASE_CHANGED_NOTIFICATION_PUSH_TOPIC_NAME = 'CaseChangedNotification';
    private static final String EMAIL_REPLY_RECEIVED_PUSH_TOPIC_NAME = 'EmailReplyReceived';
    @TestVisible private static final String CASE_CHANGED_EVENT_TYPE = 'caseChanged'; // custom String
    @TestVisible private static final String EMAIL_RECEIVED_EVENT_TYPE = 'emailReceived';  // custom String
    @TestVisible private static Map<String, String> EVENT_NAMES_AND_TYPES = new Map<String, String>{CASE_CHANGED_NOTIFICATION_PUSH_TOPIC_NAME => CASE_CHANGED_EVENT_TYPE, EMAIL_REPLY_RECEIVED_PUSH_TOPIC_NAME =>EMAIL_RECEIVED_EVENT_TYPE};

    public static Map<Id, Case> checkOfEmailSending(List<EmailMessage> sentEmails) {
        if (EXCLUDED_PROFILES.contains(PROFILE_NAME)) {
            return new Map<Id, Case>();
        }

        Map<Id, Id> caseIdWithEmailId = new Map<Id, Id>();
        for (EmailMessage email : sentEmails) {
            if (email.Incoming == false && email.Status == '3') {
                caseIdWithEmailId.put(email.ParentId, email.Id);
            }
        }

        Map<Id, Case> parentCases = new Map<Id, Case>([
                SELECT Id, Last_Email_Sent__c
                FROM Case
                WHERE Id IN : caseIdWithEmailId.keySet()
        ]);

        for (Case c : parentCases.values()) {
            c.Last_Email_Sent__c = String.valueOf(caseIdWithEmailId.get(c.Id));
        }

        return parentCases;
    }

        public static void setPushTopics() {
        String pushTopicName = '';

        List<PushTopic> pushTopics = [
                SELECT Id, Name
                FROM PushTopic
                WHERE Name IN :EVENT_NAMES_AND_TYPES.keySet()
        ];

        if (pushTopics.size() == EVENT_NAMES_AND_TYPES.keySet().size()) {
            String errorstring = 'Warning! PushTopics have already been set.\n\n'
                   + 'You can update the PushTopics via running CaseChangedNotificationUtils.updatePushTopics() method.';
            insert new ExceptionLogger__c(errorText__c = errorstring);
            NotifyAdminController.sendNotification('Class Warning Notification: CaseChangedNotificationUtils', errorstring, null);

            System.debug('Warning! PushTopics have already been set.');
            System.debug('You can update PushTopics via running  CaseChangedNotificationUtils.updatePushTopics() method.');

            return;
        }

        try {
            List<PushTopic> pts = new List<PushTopic>();

            for ( String key: EVENT_NAMES_AND_TYPES.keySet()) {
                String query = getQueryWithExcludedUserIds(EVENT_NAMES_AND_TYPES.get(key));
                    pushTopicName = key;

                PushTopic pt = new PushTopic (
                        ApiVersion = 50.0,
                        IsActive = true,
                        Name = pushTopicName,
                        NotifyForFields = 'Select',
                        NotifyForOperationUpdate = true,
                        NotifyForOperationCreate = false,
                        NotifyForOperationDelete = false,
                        NotifyForOperationUndelete = false,
                        Query = query
                );

                pts.add(pt);
            }

            insert pts;
        } catch (Exception ex) {
            String errorstring =  'Class CaseChangedNotificationUtils exception.\n\nException message:\n' + ex;
            insert new ExceptionLogger__c(errorText__c = errorstring);
            NotifyAdminController.sendNotification('Class exception Notification: CaseChangedNotificationUtils', errorstring, null);
        }
    }

    private static String getQueryWithExcludedUserIds(String eventType) {
        String query = '';
        List<String> excludedUserIdsStr = new List<String>();

        Map<Id, User> excludedUsers = new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Profile.Name IN : EXCLUDED_PROFILES
                AND isActive = TRUE
        ]);

        for (Id key : excludedUsers.keySet()) {
            excludedUserIdsStr.add(String.valueOf(key));
        }

        Map<String,String> customFieldsMap = getFieldsListFromCustomSetting();
        Map<String,String> customFiltersMap = getFiltersFromCustomSetting();

        if (eventType == CASE_CHANGED_EVENT_TYPE) {

                if (excludedUserIdsStr.isEmpty() == false) {
                    query = 'SELECT Id, CaseNumber, LastModifiedById, LastModifiedDate' + customFieldsMap.get(CASE_CHANGED_EVENT_TYPE)
                            + ' FROM Case WHERE LastModifiedById NOT IN (\'' + String.join(excludedUserIdsStr, '\', \'') + '\')' + customFiltersMap.get(CASE_CHANGED_EVENT_TYPE);

                } else {
                    query = 'SELECT Id, CaseNumber, LastModifiedById, Last_Email_Sent__c, LastModifiedDate' + customFieldsMap.get(CASE_CHANGED_EVENT_TYPE)
                            + ' FROM Case WHERE Id != null' + customFiltersMap.get(CASE_CHANGED_EVENT_TYPE);
                }
        }

        if (eventType == EMAIL_RECEIVED_EVENT_TYPE) {
                query = 'SELECT Id, CaseNumber, LastModifiedById, LastModifiedDate, Last_Incoming_Email_Timestamp__c' + customFieldsMap.get(EMAIL_RECEIVED_EVENT_TYPE)
                        + ' FROM Case WHERE Id != null' + customFiltersMap.get(EMAIL_RECEIVED_EVENT_TYPE);
        }

        return query;
    }

    private static Map<String,String> getFieldsListFromCustomSetting() {
        Map<String, CCNotification_CaseFields__c> caseFields = CCNotification_CaseFields__c.getAll();
        Map<String, String> customFieldsMap = new Map<String, String>();
        customFieldsMap.put(CASE_CHANGED_EVENT_TYPE, '');
        customFieldsMap.put(EMAIL_RECEIVED_EVENT_TYPE,'');
        String customFields = '';

        if (caseFields.size() > 0) {
            List<String> caseChangedEventFields = new List<String>();
            List<String> emailReceivedEventFields = new List<String>();

            for (CCNotification_CaseFields__c field : caseFields.values()) {
                if (field.Apply_For_Case_Changed_Event__c == true) {
                    caseChangedEventFields.add(field.Name);
                }
                if (field.Apply_For_Email_Received_Event__c == true) {
                    emailReceivedEventFields.add(field.Name);
                }
            }

            if (caseChangedEventFields.isEmpty() == false) {
                customFields = ', ' + String.join(caseChangedEventFields, ', ');
                    customFieldsMap.put(CASE_CHANGED_EVENT_TYPE, customFields);

                if  (emailReceivedEventFields.isEmpty() == false) {
                    customFields = ', ' + String.join(emailReceivedEventFields, ', ');

                    customFieldsMap.put(EMAIL_REPLY_RECEIVED_PUSH_TOPIC_NAME, customFields);

                        customFieldsMap.put(EMAIL_RECEIVED_EVENT_TYPE, customFields);
                }
            }
        }

        return customFieldsMap;
    }

    private static Map<String,String> getFiltersFromCustomSetting() {
        Map<String, CCNotification_Filters__c> filters = CCNotification_Filters__c.getAll();

        Map<String, String> customFiltersMap = new Map<String, String>();
        customFiltersMap.put(CASE_CHANGED_EVENT_TYPE, '');
        customFiltersMap.put(EMAIL_RECEIVED_EVENT_TYPE,'');

        String customFilters = '';
        List<String> caseChangedFilters = new List<String>();
        List<String> emailReceivedFilters = new List<String>();

        for (String key : filters.keySet()) {
            customFilters += ' ';
            CCNotification_Filters__c filter = filters.get(key);

            if (filter.Apply_For_Case_Changed_Event__c == true) {
                caseChangedFilters.add(' AND ' + filter.Filter_Condition__c);
            }
            if (filter.Apply_For_Email_Received_Event__c == true) {
                emailReceivedFilters.add(' AND ' + filter.Filter_Condition__c);
            }
        }

        if (caseChangedFilters.isEmpty() == false) {
            customFiltersMap.put(CASE_CHANGED_EVENT_TYPE, String.join(caseChangedFilters, ''));
        }

        if (emailReceivedFilters.isEmpty() == false) {
            customFiltersMap.put(EMAIL_RECEIVED_EVENT_TYPE, String.join(emailReceivedFilters, ''));
        }

        return customFiltersMap;
    }

    public static void updatePushTopics() {
        List<String> pushTopicNames = new List<String>(EVENT_NAMES_AND_TYPES.keySet());
        updatePushTopics(new List<String>(EVENT_NAMES_AND_TYPES.keySet()));
    }

    private static void updatePushTopics(List<String> pushTopicNames) {
        List<PushTopic> pushTopics;
        if (Test.isRunningTest() == false) {
            pushTopics = [
                    SELECT Id, Name, Query
                    FROM PushTopic
                    WHERE Name IN :pushTopicNames
            ];
        } else { // in Test
            pushTopics = [
                    SELECT Id, Name, Query
                    FROM PushTopic
                    WHERE Name IN : EVENT_NAMES_AND_TYPES.keySet()
            ];
        }

        if (pushTopics.size() < pushTopicNames.size()) {

            String errorstring = 'Class CaseChangedNotificationUtils warning.\n\nWarning! Please, create a PushTopic record using createPushTopics() method firstly.';
            insert new ExceptionLogger__c(errorText__c = errorstring);
            NotifyAdminController.sendNotification('Class Warning Notification: CaseChangedNotificationUtils', errorstring, null);

            System.debug('Warning! Please, create a PushTopic record using createPushTopics() method firstly.');

            return;
        }

        try {
            for (PushTopic pt : pushTopics) {
                pt.Query = getQueryWithExcludedUserIds(EVENT_NAMES_AND_TYPES.get(pt.Name));
            }

            update pushTopics;
        } catch (Exception ex) {
            String errorstring =  'Class CaseChangedNotificationUtils exception.\n\nException message:\n' + ex;
            insert new ExceptionLogger__c(errorText__c = errorstring);
            NotifyAdminController.sendNotification('Class exception Notification: CaseChangedNotificationUtils', errorstring, null);
        }
    }
}