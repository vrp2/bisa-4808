@IsTest
private class CaseChangedNotificationCtrl_Test {
    @TestSetup
    static void setupData() {
        CaseCustomSettingUtil.customSettingMethod();

        User testUser = TestUtils.createUser('VIP', true);

        System.runAs(testUser) {
            Id recordTypeId = [
                    SELECT Name, SobjectType,IsPersonType
                    FROM RecordType
                    WHERE SobjectType='Account'
                    AND IsPersonType = true
                    ORDER BY CreatedDate
                    LIMIT 1
            ].Id;

            Account newAccount = new Account (
                    FirstName = 'Test Account',
                    LastName = 'Account',
                    RecordTypeId = recordTypeId,
                    PersonEmail = 'test7@test.com',
                    RG_Level__c = 'Level 1',
                    AML_Rating__c = 'Rating',
                    KYC_Status__c = 'Status',
                    Segmentation__c = 'VIP',
                    Language__c = 'EN'
            );
            insert newAccount;

            Contact newContact = new Contact (
                    FirstName = 'Test Test',
                    LastName = 'Contact',
                    Email = 'test7@test.com'
            );
            insert newContact;

            Case newCase = new Case(
                    Subject = 'TestSubject',
                    Brand__c = 'Tipico',
                    Language__c = 'English',
                    Status = 'Open',
                    SuppliedEmail = 'testEmail@test.com',
                    AccountId = newAccount.Id,
                    ContactId = newContact.Id,
                    RecordTypeId = [
                            SELECT Id
                            FROM RecordType
                            WHERE DeveloperName = 'Malta_Customer_Support'
                            AND SobjectType = 'Case'
                    ].Id,
                    OwnerId = [
                            SELECT Id
                            FROM User
                            WHERE UserRole.Name = 'CO Agent'
                            AND IsActive = TRUE
                            LIMIT 1
                    ].Id
            );

            insert newCase;
        }
    }

    @IsTest
    static void getLastModifiedUserName_Test() {
        Test.startTest();
        String userName = CaseChangedNotificationCtrl.getLastModifiedUserName(UserInfo.getUserId());
        Test.stopTest();

        System.assert(userName == UserInfo.getName());
    }
}