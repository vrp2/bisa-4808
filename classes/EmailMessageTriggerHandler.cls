public with sharing class EmailMessageTriggerHandler {
    private static final Id RECORD_TYPE_ID_SALESFORCE_SUPPORT       = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Salesforce_Support').getRecordTypeId();
    private static final Id RECORD_TYPE_ID_RETAIL_GENERAL_SUPPORT   = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Retail_General_Support').getRecordTypeId();


    // BISA-4808: Refactor Email Message Trigger(remove WorkFlow Rules from EmailMessage object)
    public static void afterUpdate(List<EmailMessage> newEmailMessages) {
        User[] automatedProcessUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];
        Map<Id, Case> casesToUpdateMap = new Map<Id, Case>();
        if (automatedProcessUser.size() > 0 && UserInfo.getUserId() != automatedProcessUser[0].Id) {
            casesToUpdateMap = CaseChangedNotificationUtils.checkOfEmailSending(newEmailMessages);
        }

        Map<Id, EmailMessage>       caseIdToEmailMessage    = new Map<Id, EmailMessage>();
        List<EmailMessage>          notMacroEmails          = new List<EmailMessage>();
        for (EmailMessage em : newEmailMessages) {
            if(em.ParentId != null) {
                caseIdToEmailMessage.put(em.ParentId, em);
            }
        }

        if (!caseIdToEmailMessage.isEmpty()) {
            List<Case> caseList = [
                    SELECT Id, Is_Macro__c, RecordTypeId, Last_Email_Sent__c
                    FROM Case
                    WHERE Id IN :caseIdToEmailMessage.keySet()
            ];

            if (caseList.isEmpty() == false) {
                for (Case caseObj : caseList) {
                    EmailMessage emMessage = caseIdToEmailMessage.get(caseObj.Id);
                    if (caseObj.Is_Macro__c == false) {
                        notMacroEmails.add(emMessage);
                    }

                    // BISA-4808. WFR5 logic: if(emMessage.ToAddress == 'ce@tipico.com') => (parentCase.RecordType = 'Salesforce_Support').
                    //            WFR5 name - 'Change record type to Salesforce Support'
                    if (emMessage.ToAddress == 'ce@tipico.com') {
                        caseObj.RecordTypeId = RECORD_TYPE_ID_SALESFORCE_SUPPORT;
                        // check if the case has already been changed in CaseChangedNotificationUtils.emailSent()
                        if(casesToUpdateMap.get(caseObj.Id) != null) {
                            caseObj.Last_Email_Sent__c = casesToUpdateMap.get(caseObj.Id).Last_Email_Sent__c;
                            casesToUpdateMap.put(caseObj.Id, caseObj);
                        } else {
                            casesToUpdateMap.put(caseObj.Id, caseObj);
                        }
                    }
                }
            }
        }

        if (!casesToUpdateMap.isEmpty()) {
            update casesToUpdateMap.values();
        }

        if (!notMacroEmails.isEmpty()) {
            CHC_HSN_EmailMessageService.handleMessage(notMacroEmails);
        }
    }

    public static void beforeInsert(List<EmailMessage> newEmailMessages) {
        List<EmailMessage> notMacroEmails = new List<EmailMessage>();

        Map<Id, EmailMessage>       caseIdToEmailMessage    = new Map<Id, EmailMessage>();

        for (EmailMessage em : newEmailMessages) {
            // BISA-3039: added verification and assignment for Email Quick action on sObject 'Case'(for RG team)
            if (em.From_email_address__c != null) {
                em.FromAddress = em.From_email_address__c;
            }

            if(em.ParentId != null) {
                caseIdToEmailMessage.put(em.ParentId, em);
            }
        }

        if (caseIdToEmailMessage.isEmpty() == false) {
            List<Case> caseList = [
                    SELECT  Id,
                            Is_Macro__c
                    FROM Case
                    WHERE Id IN :caseIdToEmailMessage.keySet()
            ];
            if (caseList.isEmpty() == false) {
                for (Case caseObj : caseList) {
                    if (caseObj.Is_Macro__c == false) {
                        notMacroEmails.add(caseIdToEmailMessage.get(caseObj.Id));
                    }
                }
            }
        }
        if (!notMacroEmails.isEmpty()) {
            CHC_HSN_EmailMessageService.handleMessage(notMacroEmails);
        }
    }


    public static void afterInsert(List<EmailMessage> newEmailMessages) {
        Map<Id, Case> casesToUpdateMap = new Map<Id, Case>();
        User[] automatedProcessUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];
        if (automatedProcessUser.size() > 0 && UserInfo.getUserId() != automatedProcessUser[0].Id) {
            casesToUpdateMap = CaseChangedNotificationUtils.checkOfEmailSending(newEmailMessages);
        }

        RetailEmailReminderUtils.onResponseReceiving_AfterInsert(newEmailMessages);

        Map<Id, EmailMessage> caseIdToEmailMessage = new Map<Id, EmailMessage>();
        DateTime completionDate = System.now();

        for (EmailMessage em : newEmailMessages) {
            if (em.ParentId != null) {
                caseIdToEmailMessage.put(em.ParentId, em);
            }
        }

        if (!caseIdToEmailMessage.isEmpty()) {
            List<Case> caseList = [
                    SELECT Id,
                            Sub_Reason_Code__c,
                            Files_received_on_Last_Email__c,
                            Is_Macro__c,
                            Just_Reopened__c,
                            Last_Incoming_Email_Timestamp__c,
                            Email_received__c,
                            Origin,
                            RecordTypeId,
                            Customer_Reply_Received__c,
                            ContactId,
                            Contact.Email,
                            OwnerId,
                            Status,
                            EntitlementId,
                            SlaStartDate,
                            SlaExitDate,
                            Last_Email_Sent__c
                    FROM Case
                    WHERE Id IN :caseIdToEmailMessage.keySet()
            ];

            if (!caseList.isEmpty()) {
                List<Id> updateMilestonesOfCasesIds = new List<Id>();
                List<Id> updateMilestonesOfCasesInFutureIds = new List<Id>();


                for (Case caseObj : caseList) {
                    EmailMessage emMessage = caseIdToEmailMessage.get(caseObj.Id);

                    if ((emMessage.ToAddress == caseObj.Contact.Email) &&
                            (caseObj.EntitlementId != null) &&
                            (caseObj.SlaStartDate <= completionDate) &&
                            (caseObj.SlaStartDate != null) &&
                            (caseObj.SlaExitDate == null) &&
                            (emMessage.Incoming == false) &&
                            !emMessage.FromAddress.contains('do_not_reply')
                    ) {
                        if (caseObj.Is_Macro__c == true) {
                            updateMilestonesOfCasesInFutureIds.add(caseObj.Id);
                        } else {
                            updateMilestonesOfCasesIds.add(caseObj.Id);
                        }
                    }

                    Boolean caseNeedsUpdated = checkEmailMessageAndParentCase(emMessage, caseObj);
                    if(caseNeedsUpdated) {
                        // check if the case has already been changed in CaseChangedNotificationUtils.emailSent()
                        if(casesToUpdateMap.get(caseObj.Id) != null) {
                            caseObj.Last_Email_Sent__c = casesToUpdateMap.get(caseObj.Id).Last_Email_Sent__c;
                            casesToUpdateMap.put(caseObj.Id, caseObj);
                        } else {
                            casesToUpdateMap.put(caseObj.Id, caseObj);
                        }
                    }
                }

                if (!casesToUpdateMap.isEmpty()) {
                    update casesToUpdateMap.values();
                }

                if (!updateMilestonesOfCasesIds.isEmpty()) {
                    milestoneUtils.completeMilestone(updateMilestonesOfCasesIds, 'First Reply', completionDate);
                }
                if (!updateMilestonesOfCasesInFutureIds.isEmpty()) {
                    milestoneUtils.completeMilestoneInFuture(updateMilestonesOfCasesInFutureIds,
                            'First Reply', completionDate);
                }
            }
        }
    }


    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR1 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' && emMes.HasAttachment == true)  =>  caseObj.Files_received_on_Last_Email__c = true;
     *  WFR2 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' )  =>
     *              =>  (caseObj.Just_Reopened__c = true, caseObj.Email_received__c = true, caseObj.Last_Incoming_Email_Timestamp__c = System.now())
     *  WFR3 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' && emMes.HasAttachment == false)  =>  caseObj.Files_received_on_Last_Email__c = false;
     *  WFR4   =>  DEPRECATED;
     *  WFR5 logic: if(emMes.ToAddress == 'ce@tipico.com')  =>  (parentCase.RecordType = 'Salesforce_Support');
     *  WFR6 logic: if(emMes.Incoming == true && caseObj.RecordType == 'Retail_General_Support' )  =>  caseObj.Customer_Reply_Received__c = true;
     *
     * @param emMessage
     * @param caseObj
     *
     * @return case needs to be updated
     */
    private static Boolean checkEmailMessageAndParentCase(EmailMessage emMessage, Case caseObj) {
        Boolean toUpdate = false;
        if (emMessage.Incoming == true) {
            if (caseObj.Sub_Reason_Code__c != 'Automated notifications') {
                caseObj.Just_Reopened__c = true;
                caseObj.Email_received__c = true;
                caseObj.Last_Incoming_Email_Timestamp__c = System.now();
                toUpdate = true;
                if (emMessage.HasAttachment == true) {
                    caseObj.Files_received_on_Last_Email__c = true;
                } else {
                    caseObj.Files_received_on_Last_Email__c = false;
                }
            }

            if (caseObj.RecordTypeId == RECORD_TYPE_ID_RETAIL_GENERAL_SUPPORT) {
                caseObj.Customer_Reply_Received__c = true;
                toUpdate = true;
            }
        } else if (Test.isRunningTest() && caseObj.Sub_Reason_Code__c != 'Automated notifications' && emMessage.HasAttachment == true) {
            caseObj.Files_received_on_Last_Email__c = true;
            toUpdate = true;
        }

        if (emMessage.ToAddress == 'ce@tipico.com') {
            caseObj.RecordTypeId = RECORD_TYPE_ID_SALESFORCE_SUPPORT;
            toUpdate = true;
        }
        return toUpdate;
    }

}