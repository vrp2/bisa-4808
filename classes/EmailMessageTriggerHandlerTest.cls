@IsTest
public with sharing class EmailMessageTriggerHandlerTest {
    @TestSetup
    public static Void prepareData() {
        CaseCustomSettingUtil.customSettingMethod();
        List<SlaProcess> entitlementProcess = [SELECT Name FROM SlaProcess WHERE Name = 'Entitlement For New Cases'];

        Id recordTypeId = [
                SELECT Name, SobjectType,IsPersonType
                FROM RecordType
                WHERE SobjectType='Account'
                AND IsPersonType = true
                ORDER BY CreatedDate
                LIMIT 1].Id;

        Account newAccount = new Account (FirstName = 'Kevin Test', LastName = 'Account', RecordTypeId = recordTypeId, PersonEmail = 'test3@test.com');
        insert newAccount;

        Contact newContact = new Contact (FirstName = 'Kevin Test', LastName = 'Contact', Email = 'test3@test.com');
        insert newContact;

        Entitlement testEntitlement = new Entitlement(Name = 'Test', AccountId = newAccount.Id, SlaProcessId = entitlementProcess[0].Id);
        insert testEntitlement;

        Case newCase = new Case();
        newCase.Subject         = 'sub1';
        newCase.Status          = 'Open';
        newCase.EntitlementId   = testEntitlement.Id;
        newCase.SlaStartDate    = Datetime.now().addDays(-7);
        newCase.AccountId       = newAccount.Id;
        newCase.ContactId       = newContact.Id;
        insert newCase;
    }

    @isTest
    public static Void testEmailMessageTrigger() {
        Id caseId = [SELECT Id FROM Case WHERE Subject = 'sub1'][0].Id;

        Test.startTest();

        EmailMessage newMessage = new EmailMessage();
        newMessage.FromAddress = 'test@test.com';
        newMessage.CcAddress = 'test2@test.com';
        newMessage.FromName = 'Mr. Test';
        newMessage.Headers = 'test';
        newMessage.HtmlBody = 'test';
        newMessage.TextBody = 'test';
        newMessage.ParentId = caseId;
        newMessage.Status = '1';
        newMessage.Subject = 'test';
        newMessage.ToAddress = 'test3@test.com';
        newMessage.Incoming = false;
        insert newMessage;

        Test.stopTest();
        List<CaseMilestone> milestoneToCheck = [
            SELECT Id, CompletionDate, IsCompleted, MilestoneType.Name
            FROM CaseMilestone
            WHERE CaseId = :caseId
                AND MilestoneType.Name = 'First Reply'
        ];

        System.assertEquals(true, milestoneToCheck[0].IsCompleted);
    }

    // testing the logic added for BISA-3039(custom dropdown menu with FromEmail addresses for RG team)
    @IsTest
    public static void testEmailMessageTriggerChangedFromEmailAddress() {
        Case caseRec = [SELECT Id FROM Case WHERE Subject = 'sub1'][0];
        String fromAddress = 'complaints@tipico.com';

        EmailMessage newMessage = createIncomeEmailMessage(caseRec, 'test@test.com', 'to_test@gmail.com', fromAddress);

        EmailMessage emailMessage = [SELECT id, FromAddress FROM EmailMessage WHERE id = :newMessage.Id];
        System.assertEquals(fromAddress, emailMessage.FromAddress);
    }


    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR1 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' && emMes.HasAttachment == true)  =>  caseObj.Files_received_on_Last_Email__c = true;
     */
    @isTest
    static void WFR1_TestMethod() {
        Case caseRec = [SELECT Id FROM Case WHERE Subject = 'sub1'][0];
        caseRec.Sub_Reason_Code__c = 'Username or Password Enquiry';
        caseRec.Files_received_on_Last_Email__c = false;
        update caseRec;

        createEmailMessageWithAttachment(caseRec); //Send Email

        Case caseUpdated = [SELECT Id, Files_received_on_Last_Email__c FROM Case WHERE Id = :caseRec.Id][0];
        System.assertEquals(true, caseUpdated.Files_received_on_Last_Email__c);
    }


    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR2 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' )  =>
     *              =>  (caseObj.Just_Reopened__c = true, caseObj.Email_received__c = true, caseObj.Last_Incoming_Email_Timestamp__c = System.now())
     */
    @isTest
    static void WFR2_TestMethod() {
        Case caseRec = [SELECT Id FROM Case WHERE Subject = 'sub1'][0];
        caseRec.Sub_Reason_Code__c = 'Username or Password Enquiry';
        caseRec.Files_received_on_Last_Email__c = false;
        caseRec.Email_received__c = false;
        caseRec.Just_Reopened__c = false;
        update caseRec;

        Test.startTest();
        createIncomeEmailMessage(caseRec, 'test@gmail.com', 'to_test@gmail.com', null);
        Test.stopTest();

        Case caseUpdated = [
                SELECT Id, Sub_Reason_Code__c, Files_received_on_Last_Email__c, Just_Reopened__c, Last_Incoming_Email_Timestamp__c, Email_received__c
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(false, caseUpdated.Just_Reopened__c);
        System.assertEquals(true, caseUpdated.Email_received__c);
        System.assert(caseUpdated.Last_Incoming_Email_Timestamp__c != null);
    }

    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR3 logic: if(emMes.Incoming == true && caseObj.Sub_Reason_Code__c != 'Automated notifications' && emMes.HasAttachment == false)  =>  caseObj.Files_received_on_Last_Email__c = false;
     */
    @isTest
    static void WFR3_TestMethod() {
        Case caseRec = [SELECT Id FROM Case WHERE Subject = 'sub1'][0];
        caseRec.Sub_Reason_Code__c = 'Username or Password Enquiry';
        caseRec.Files_received_on_Last_Email__c = true;
        update caseRec;

        Test.startTest();
        createIncomeEmailMessage(caseRec, 'test@gmail.com', 'to_test@gmail.com', null);
        Test.stopTest();

        Case caseUpdated = [
                SELECT Id, Sub_Reason_Code__c, Files_received_on_Last_Email__c, Just_Reopened__c, Last_Incoming_Email_Timestamp__c, Email_received__c
                FROM Case
                WHERE Id = :caseRec.Id
        ];
        System.assertEquals(false, caseUpdated.Files_received_on_Last_Email__c);
    }

    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR5 logic: if(emMes.ToAddress == 'ce@tipico.com')  =>  (parentCase.RecordType = 'Salesforce_Support');
     */
    @isTest
    static void WFR5_afterInsert_TestMethod() {
        Case caseRec = [SELECT Id, RecordTypeId FROM Case WHERE Subject = 'sub1'][0];

        Test.startTest();
        createIncomeEmailMessage(caseRec, 'test@gmail.com', 'ce@tipico.com', null);

        Test.stopTest();

        RecordType recType = [SELECT Id, Name from RecordType where Name = 'Salesforce Support' LIMIT 1];


        Case caseUpdated = [
                SELECT Id, RecordtypeId, Recordtype.Name
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(recType.Id, caseUpdated.RecordtypeId);
    }

    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR5 logic: if(emMes.ToAddress == 'ce@tipico.com')  =>  (parentCase.RecordType = 'Salesforce_Support');
     */
    @isTest
    static void WFR5_afterUpdate_TestMethod() {
        Case caseRec = [SELECT Id, RecordTypeId FROM Case WHERE Subject = 'sub1'][0];
        createIncomeEmailMessage(caseRec, 'test@gmail.com', 'to_test@gmail.com', null);

        Test.startTest();
        List<EmailMessage> emailMessages = [SELECT Id, ToAddress FROM EmailMessage];
        emailMessages[0].ToAddress = 'ce@tipico.com';
        update emailMessages;
        Test.stopTest();

        RecordType recType = [SELECT Id, Name from RecordType where Name = 'Salesforce Support' LIMIT 1];


        Case caseUpdated = [
                SELECT Id, RecordtypeId, Recordtype.Name
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(recType.Id, caseUpdated.RecordtypeId);
    }

    /**
     *  WFR6 logic: if(emMes.Incoming == true && caseObj.RecordType == 'Retail_General_Support' )  =>  caseObj.Customer_Reply_Received__c = true;
     */
    @isTest
    static void WFR6_TestMethod() {
        RecordType recType = [SELECT Id, Name from RecordType where Name ='Retail General Support' LIMIT 1];

        Case caseRec = [SELECT Id, RecordTypeId FROM Case WHERE Subject = 'sub1'][0];
        caseRec.RecordTypeId = recType.Id;
        update caseRec;


        Test.startTest();
        createIncomeEmailMessage(caseRec, 'test@gmail.com', 'to_test@gmail.com', null);
        Test.stopTest();

        Case caseUpdated = [
                SELECT Id, RecordtypeId, Customer_Reply_Received__c
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(true, caseUpdated.Customer_Reply_Received__c);
    }

    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     *  WFR5 logic: if(emMes.ToAddress == 'ce@tipico.com')  =>  (parentCase.RecordType = 'Salesforce_Support');
     *   and CaseChangedNotificationUtils.checkOfEmailSending() : if(emMes.Incoming == false && emMes.Status == '3')  =>  c.Last_Email_Sent__c = String.valueOf(emMes.Id);
     */
    @IsTest
    static void WFR5_afterUpdate_checkOfEmailSending() {
        Case caseRec = [SELECT Id, RecordTypeId, Last_Email_Sent__c FROM Case WHERE Subject = 'sub1'][0];

        EmailMessage newMessage = new EmailMessage();
        newMessage.FromAddress = 'test@test.com';
        newMessage.CcAddress = 'test2@test.com';
        newMessage.FromName = 'Mr. Test';
        newMessage.Headers = 'test';
        newMessage.HtmlBody = 'test';
        newMessage.TextBody = 'test';
        newMessage.ParentId = caseRec.id;
        newMessage.Status = '1';
        newMessage.Subject = 'test';
        newMessage.ToAddress = 'test3@tipico.com';
        newMessage.Incoming = false;
        insert newMessage;

        Test.startTest();
        List<EmailMessage> emailMessages = [SELECT Id, ToAddress, Status FROM EmailMessage];
        emailMessages[0].ToAddress = 'ce@tipico.com';
        emailMessages[0].Status = '3';
        update emailMessages;
        Test.stopTest();

        RecordType recType = [SELECT Id, Name from RecordType where Name ='Salesforce Support' LIMIT 1];

        Case caseUpdated = [
                SELECT Id, RecordtypeId, Last_Email_Sent__c
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(recType.Id, caseUpdated.RecordtypeId);
        System.assertEquals(newMessage.Id, caseUpdated.Last_Email_Sent__c);
    }

    /**
     * Description: BISA-4808. Moved logic from WorkFlow Rules to Apex
     * CaseChangedNotificationUtils.checkOfEmailSending() : if(emMes.Incoming == false && emMes.Status == '3')  =>  c.Last_Email_Sent__c = String.valueOf(emMes.Id);
     */
    @IsTest
    static void afterUpdate_CaseChangedNotificationUtils() {
        Case caseRec = [SELECT Id, RecordTypeId, Last_Email_Sent__c FROM Case WHERE Subject = 'sub1'][0];

        EmailMessage newMessage = new EmailMessage();
        newMessage.FromAddress = 'test@test.com';
        newMessage.CcAddress = 'test2@test.com';
        newMessage.FromName = 'Mr. Test';
        newMessage.Headers = 'test';
        newMessage.HtmlBody = 'test';
        newMessage.TextBody = 'test';
        newMessage.ParentId = caseRec.id;
        newMessage.Status   = '3';
        newMessage.Subject  = 'test';
        newMessage.ToAddress = 'test3@tipico.com';
        newMessage.Incoming = false;
        insert newMessage;

        Case caseUpdated = [
                SELECT Id, RecordtypeId, Last_Email_Sent__c, RecordType.Name
                FROM Case
                WHERE Id = :caseRec.Id
        ];

        System.assertEquals(newMessage.Id, caseUpdated.Last_Email_Sent__c);
    }

    private static void createEmailMessageWithAttachment(Case caseRec) {
        Document documentObj;
        documentObj = new Document();
        documentObj.Body = Blob.valueOf('Some Document Text');
        documentObj.ContentType = 'application/pdf';
        documentObj.DeveloperName = 'my_document';
        documentObj.IsPublic = true;
        documentObj.Name = 'My Document';
        documentObj.FolderId = [select id from folder WHERE name = 'Communities Shared Document Folder'][0].id;
        insert documentObj;


        //Get your document from document Object
        Document doc = [Select Id, Name, Body, ContentType, DeveloperName, Type From Document where Id = :documentObj.Id];

        //Create Email file attachment from document
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType(doc.ContentType);
        attach.setFileName(doc.DeveloperName + '.' + doc.Type);
        attach.setInline(false);
        attach.Body = doc.Body;


        //Apex Single email message
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[]{
                'itzbiswajeet11@gmail.com'
        });//Set To Email Address
        mail.setSubject('Test Email With Attachment');//Set Subject
        mail.setHtmlBody('Please find the attachment.');//Set HTML Body
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{
                attach
        });  //Set File Attachment
        mail.setWhatId(caseRec.Id);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                mail
        });
    }

    private static EmailMessage createIncomeEmailMessage(Case caseRec, String fromAddress, String toAddress, String From_email_address) {
        EmailMessage emailMessage = new EmailMessage();

        emailMessage.FromAddress  = fromAddress;
        emailMessage.CcAddress    = 'test2@test.com';
        emailMessage.FromName     = 'Mr. Test';
        emailMessage.Headers      = 'test';
        emailMessage.HtmlBody     = 'test';
        emailMessage.TextBody     = 'test';
        emailMessage.ParentId     = caseRec.Id;
        emailMessage.Status       = '1';
        emailMessage.Subject      = 'test';
        emailMessage.ToAddress    = toAddress;
        emailMessage.Incoming     = true;
        emailMessage.From_email_address__c = From_email_address;

        insert emailMessage;

        return emailMessage;
    }

}