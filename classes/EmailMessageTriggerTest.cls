@isTest
public class EmailMessageTriggerTest {

    @testSetup 
    public static Void prepareData() {
        CaseCustomSettingUtil.customSettingMethod();
        List<SlaProcess> entitlementProcess = [SELECT Name FROM SlaProcess WHERE Name = 'Entitlement For New Cases'];

        Id recordTypeId = [
            SELECT Name, SobjectType,IsPersonType 
            FROM RecordType 
            WHERE SobjectType='Account' 
                AND IsPersonType = true 
                ORDER BY CreatedDate
                LIMIT 1].Id;

        Account newAccount = new Account (FirstName = 'Kevin Test', LastName = 'Account', RecordTypeId = recordTypeId, PersonEmail = 'test3@test.com');
        insert newAccount;

        Contact newContact = new Contact (FirstName = 'Kevin Test', LastName = 'Contact', Email = 'test3@test.com');
        insert newContact;

        Entitlement testEntitlement = new Entitlement(Name = 'Test', AccountId = newAccount.Id, SlaProcessId = entitlementProcess[0].Id);
        insert testEntitlement;

        Case newCase = new Case();
            newCase.Subject = 'sub1';           
            newCase.Status = 'Open'; 
            newCase.EntitlementId = testEntitlement.Id;                 
            newCase.SlaStartDate = Datetime.now().addDays(-7);
            newCase.AccountId = newAccount.Id;
            newCase.ContactId = newContact.Id;
        insert newCase;
    }

    @isTest
    public static Void testEmailMessageTrigger() {  
        Id caseId = [SELECT Id FROM Case WHERE Subject = 'sub1'][0].Id;  

        Test.startTest();
            
            EmailMessage newMessage = new EmailMessage();
                newMessage.FromAddress = 'test@test.com';           
                newMessage.CcAddress = 'test2@test.com';   
                newMessage.FromName = 'Mr. Test';   
                newMessage.Headers = 'test';
                newMessage.HtmlBody = 'test';      
                newMessage.TextBody = 'test';
                newMessage.ParentId = caseId;
                newMessage.Status = '1';
                newMessage.Subject = 'test';
                newMessage.ToAddress = 'test3@test.com';   
                newMessage.Incoming = false;
            insert newMessage;

        Test.stopTest();
        /*
        List<CaseMilestone> milestoneToCheck = [
            SELECT Id, CompletionDate, IsCompleted, MilestoneType.Name 
            FROM CaseMilestone
            WHERE CaseId = :caseId
                AND MilestoneType.Name = 'First Reply'                          
        ];

        System.assertEquals(true, milestoneToCheck[0].IsCompleted);
        */
    }

    // testing the logic added for BISA-3039(custom dropdown menu with FromEmail addresses for RG team)
    @isTest
    public static Void testEmailMessageTriggerChangedFromEmailAddress() {
        Id caseId = [SELECT Id FROM Case WHERE Subject = 'sub1'][0].Id;
        String fromAddress = 'test@test.com';
        String fromAddress2 = 'test2@test.com';
        EmailMessage newMessage = new EmailMessage();

        newMessage.FromAddress = fromAddress;
        newMessage.CcAddress = 'test2@test.com';
        newMessage.FromName = 'Mr. Test';
        newMessage.Headers = 'test';
        newMessage.HtmlBody = 'test';
        newMessage.TextBody = 'test';
        newMessage.ParentId = caseId;
        newMessage.Status = '1';
        newMessage.Subject = 'test';
        newMessage.ToAddress = 'test3@test.com';
        newMessage.Incoming = false;
        newMessage.From_email_address__c = 'complaints@tipico.com';

        insert newMessage;

        EmailMessage emailMessage = [SELECT id, FromAddress FROM EmailMessage WHERE id = :newMessage.Id];
        System.assertEquals(newMessage.From_email_address__c, emailMessage.FromAddress);

        newMessage.FromAddress = fromAddress2;

        Test.startTest();
        update newMessage;
        Test.stopTest();

        EmailMessage emailMessage2 = [SELECT id, FromAddress FROM EmailMessage WHERE id = :newMessage.Id];
        System.assertEquals(fromAddress2, emailMessage2.FromAddress);

    }
}